
    

   
   
   
$(document).ready(function(){
    // === Menu script ====
    let show_menu = false;
    let show_user = false;


    // $('.user-image').click(function(){
    //     $('.j-nav').removeClass('is-visible');
    //     show_menu = false;
    //     if(show_user){
    //         $('#user').removeClass('is-visible');
    //         show_user = false;
    //     } else{
    //         $('#user').addClass('is-visible');
    //         show_user = true;
    //     }
    // });


    $('.j-menu-bars').click(function(){
        show_user = false;
        if(show_menu){
            $('.j-nav').slideUp(500).removeClass('is-visible');
            show_menu = false;
        } else{
            $('.j-nav').addClass('is-visible').slideDown(500);
            show_menu = true;
        }
    });

    $('.j-nav a').click(function(){
        $('.j-nav a').removeClass('active');
        $(this).addClass('active');
        $('.j-nav').removeClass('is-visible');
        show_menu = false;
        if($(window).width() < 500)
            {
                $('.j-nav').slideUp(500).removeClass('is-visible');
            }
    });
    // == End of menu script ====
    
    $(window).scroll(function(){
       if($(this).scrollTop() > 50){
            $('.scroll-up').fadeIn();
            $('.j-header').addClass('colored');
       } else{
            $('.scroll-up').fadeOut();
            $('.j-header').removeClass('colored');
       }
    });
    $('.scroll-up').fadeOut();
    $('.scroll-up').click(function(){
        $('html ,body').animate({scrollTop : 0},800);
    });
});